golang-github-vmware-govmomi (0.24.2-3) unstable; urgency=medium

  * Team upload
  * Fix x509.UnknownAuthorityError detection in vim25 package as well
  * Skip TestReadV3 which needs non empty /etc/resolv.conf

 -- Shengjing Zhu <zhsj@debian.org>  Wed, 22 Feb 2023 16:48:46 +0800

golang-github-vmware-govmomi (0.24.2-2) unstable; urgency=medium

  * Team upload
  * Update Standards-Version to 4.6.2 (no changes)
  * Add Multi-Arch hint
  * Skip TestSessionManagerLoginExtension which accesses network
  * Fix x509.UnknownAuthorityError detection
  * Add patch to skip flaky ExampleHandlerREST

 -- Shengjing Zhu <zhsj@debian.org>  Thu, 19 Jan 2023 20:07:39 +0800

golang-github-vmware-govmomi (0.24.2-1) unstable; urgency=medium

  * Team upload.
  * Fix uscan watch file
  * Update Section to golang
  * Update Standards-Version to 4.6.0 (no changes)
  * New upstream version 0.24.2
    + Fix tests with Go1.17 (Closes: #997554)
  * Add new fixtures to DH_GOLANG_INSTALL_EXTRA
  * Exclude toolbox package to reduce Build-Depends
  * Add patch to disable test which needs git command

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 24 Oct 2021 23:20:55 +0800

golang-github-vmware-govmomi (0.23.0-1) unstable; urgency=medium

  * Team upload.

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Set upstream metadata fields.

  [ Shengjing Zhu ]
  * New upstream version 0.23.0
  * Update Build-Depends and Depends for new version
  * Update maintainer address to team+pkg-go@tracker.debian.org
  * Update Standards-Version to 4.5.0 (no changes)
  * Add Rules-Requires-Root and autopkgtest-pkg-go
  * Exclude vendor dir
  * Exclude toolbox, vcsim and all examples
  * Disable unreliable race test
  * Add patch to fix source file permission

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 21 Jun 2020 20:01:54 +0800

golang-github-vmware-govmomi (0.15.0-1) unstable; urgency=medium

  * New upstream version.
  * Update debian/copyright.

 -- Andrew Shadura <andrewsh@debian.org>  Mon, 25 Sep 2017 07:58:32 +0200

golang-github-vmware-govmomi (0.8.0+git20160719.9.b5ee639-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Tim Potter <tpot@hpe.com>  Wed, 12 Apr 2017 11:55:56 +1000

golang-github-vmware-govmomi (0.8.0-1) unstable; urgency=medium

  * Initial release (Closes: #821279).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 03 Jul 2016 22:45:54 +1000
